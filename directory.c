//
//  tFat.c
//  FatFileSystem
//
//  Created by Axat on 2012-11-20.
//  Copyright (c) 2012 Axat. All rights reserved.
//

#include "directory.h"
#include "fatfs.h"
#include "direntry.h"

/*
 * 1 if lastCluster else 0
 */
int islastcluster(int clusternum,unsigned int *fat) {
    if (fat[clusternum] == 0xfff)
        return 1;
    else
        return 0;
}

/*
 *  Get next Cluster given the current cluster
 */
int getnextcluster(int clusternum,unsigned int *fat) {
    return fat[clusternum];
}

/*
 * List all clusters
 */
void listclusters(int clusternum,unsigned int *fat) {
    if(islastcluster(clusternum,fat)) {
        printf("%d",clusternum);
        return;
    }
    else
        printf("%d,",clusternum);
    listclusters((getnextcluster(clusternum,fat)),fat);
    return;
}

//Copies the cluster to a buffer
void copycluster(int fd,int clusternumber,unsigned char  *clusterbuffer,fatfsinfo *fatfsinfo) {
    
    int clusterSeek = (fatfsinfo->sectorNumberDataCluster*fatfsinfo->sectorSize) + (clusternumber - 2) * (fatfsinfo->clusterSizeBytes);
    //Set the seek to the given cluster number
    if(lseek(fd,clusterSeek,SEEK_SET) < 0) return;
    
    //Read one cluster from the data area
    if(read(fd,clusterbuffer,fatfsinfo->clusterSizeBytes) != fatfsinfo->clusterSizeBytes)
        return;
}

/*
 * Makes a dir path
 */
char* makeDirpath(char *parent, char *child,int size){
    char *str = (char *)malloc(size);
    strcat(str,parent);
    strcat(str, child);
    strcat(str, "/");
    
    return str;
}

/*
 * Makes and prints a file path
 */
void printFilePath(char *pname,char *filename, char *ext) {
    printf("%s",pname);
    printname(filename, ext);
}
/*
 Read all the directory entries and output information
 */
void traverseFat(char *pName, int fd,unsigned char *buffer,int clusternumber,unsigned int *Fat,fatfsinfo *fatfsinfo) {
    int clustersize = fatfsinfo->clusterSizeBytes;
    int i;
    for (i = 0; i < clustersize; i = i + ROOTDIR_ENTRY_SIZE) {
        char *name;
        
        unsigned char *clusterbuffer = (unsigned char *) malloc(clustersize);
        if(isDirEnd(buffer,i))
            break;
        else if (isDir(buffer,i)) {
            if(buffer[i] == 0x2E || buffer[i + 1] == 0x2E) {
                printf("%s",pName);
                name = getName(buffer,i);
                printf("%s",name);
                printnewline();
                free(name);
                continue;
            }
        else
            printf("%s",pName);
            int fclusnumber = getFirstCluster(buffer,i);
            copycluster(fd,fclusnumber,clusterbuffer,fatfsinfo);
            name = getName(buffer,i);
            printf("%s",name);
            int length = (int)(strlen(pName)+strlen(name)+1);
            name = makeDirpath(pName,name,length);
            printf(" || List of Clusters: ");
            listclusters(fclusnumber,Fat);
            printnewline();
            traverseFat(name, fd, clusterbuffer,fclusnumber,Fat,fatfsinfo);
            while(!(islastcluster(fclusnumber,Fat))) {
                fclusnumber = getnextcluster(fclusnumber,Fat);
                copycluster(fd,fclusnumber,clusterbuffer,fatfsinfo);
                traverseFat(name,fd,clusterbuffer,fclusnumber,Fat,fatfsinfo);
           }
        }
        else if(!isDeletedFile(buffer, i)) {
            char *ext;
            name = getName(buffer,i);
            ext = getExt(buffer,i);
            int fclusnumber = getFirstCluster(buffer,i);
            printFilePath(pName,name,ext);
            printf(" ||List of Clusters: ");
            listclusters(fclusnumber,Fat);
            printnewline();
            }
        else
            continue;
    }
}