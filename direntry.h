//
//  direntry.h
//  FatFileSystem
//
//  Created by Axat on 2012-11-20.
//  Copyright (c) 2012 Axat. All rights reserved.
//

#ifndef DIRENTRY_H
#define DIRENTRY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Trims trailing white space in a given string
 */
char *trimtrailwhitespace(char *str);

/*
 *Returns 1 if end of Directory else returns 0
 */
int isDirEnd(unsigned char* buffer, int i);

/*
 * Returns 1 if file has been marked for deletion, else returns 0
 */
int isDeletedFile(unsigned char *buffer, int i);

/*
 * Returns 1 if is a directory else returns 0
 */
int isDir(unsigned char *buffer,int i);

/*
 * Returns 1 if valid filename, else returns 0
 */
int isValidName(unsigned char *buffer, int i,int length);

/*
 * Gets the name of the directory and stores it in char array
 */
char* getName(unsigned char *buffer,int i);

/*
 * Gets the extension of the file and stores it in char array
 */
char* getExt(unsigned char *buffer,int i);

/*
 * Returns the first cluster number of a file/directory
 */
int getFirstCluster(unsigned char *buffer,int i);

/*
 * Prints the name of the given directory or file depending on the extension
 */
void printname(char *name,char *ext);

/*
 * Prints a newline
 */
void printnewline(void);

/*
 *Read the root directory entries
 */
void readRootDirEntries(unsigned char *buffer,size_t bufsize);

#endif
