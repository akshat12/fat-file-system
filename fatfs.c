

#include "fatfs.h"
/*
 * A function which gets all of the boot sector info from fat_image.dat
 */
void readBootSector(fatfsinfo *fatfsinfo, unsigned char* buffer) {
    fatfsinfo->sectorSize = (buffer[12] << 8) | buffer[11];
    printf("Sector Size: %d \n",fatfsinfo->sectorSize);
    
    fatfsinfo->clusterSize = buffer[13];
    printf("Cluster Size in Sectors: %d \n",fatfsinfo->clusterSize);
    
    fatfsinfo->rootDirEntries = (buffer[18] << 8) | buffer[17];
    printf("Number of Root Directory Entries: %d \n",fatfsinfo->rootDirEntries);
    
    fatfsinfo->fatSectorSize = (buffer[23] << 8) | buffer[22];
    printf("Numbers of Sectors per FAT : %d\n",fatfsinfo->fatSectorSize);
    
    fatfsinfo->reservedSectors = (buffer[15] << 8) | buffer[14];
    printf("Number Reserved Sectors : %d\n",fatfsinfo->reservedSectors);
    
    fatfsinfo->hiddenSectors = (buffer[29] << 8) | buffer[28];
    printf("Number of Hidden Sectors : %d\n" ,fatfsinfo->hiddenSectors);
    
    fatfsinfo->firstFATSectorNumber = 0 + fatfsinfo->reservedSectors;
    printf("Sector Number of first copy of FAT : %d\n" ,fatfsinfo->firstFATSectorNumber);
    
    fatfsinfo->numberFATCopies = buffer[16];
    
    fatfsinfo->firstSecNumberRoot = fatfsinfo->reservedSectors + (fatfsinfo->fatSectorSize*fatfsinfo->numberFATCopies);
    printf("Sector number of first sector of root directory : %d\n" ,fatfsinfo->firstSecNumberRoot);
    
    //Gives you the number of sectors occupied by the directory entries
    fatfsinfo->rootDirSectorSize = (fatfsinfo->rootDirEntries * ROOTDIR_ENTRY_SIZE)/fatfsinfo->sectorSize;
    fatfsinfo->sectorNumberDataCluster = fatfsinfo->rootDirSectorSize + fatfsinfo->firstSecNumberRoot;
    printf("Sector number of first sector of the first usable data cluster : %d\n" , fatfsinfo->sectorNumberDataCluster);
    
    fatfsinfo->rootDirSizeBytes = fatfsinfo->rootDirSectorSize * fatfsinfo->sectorSize;
    
    fatfsinfo->clusterSizeBytes = fatfsinfo->clusterSize*fatfsinfo->sectorSize;
}