//
//  fat.h
//  FatFileSystem
//
//  Created by Axat on 2012-11-19.
//  Copyright (c) 2012 Axat. All rights reserved.
//

#ifndef FAT_H
#define FAT_H

#include <stdio.h>
#include <stdlib.h>

/*
 * Process the fat and stores all the entries in an integer array (FAT table)
 *
 */
void processFat(unsigned int *fat,unsigned char *fatbuffer, size_t bufsize);

#endif
