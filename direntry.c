//
//  direntry.c
//  FatFileSystem
//
//  Created by Axat on 2012-11-19.
//  Copyright (c) 2012 Axat. All rights reserved.
//


#include "direntry.h"
#include "fatfs.h"



/*
 * Trims trailing white space in a given string
 */
char *trimtrailwhitespace(char *str)
{
    //Trim trailing white space
    char *end;
    end = str;
    end = str + strlen(str) - 1;
    while (*end && *end == ' ') end--;
    //Write new null terminator
    *(end + 1) = '\0';
    
    return str;
}

/*
 *Returns 1 if end of Directory else returns 0
 */
int isDirEnd(unsigned char* buffer, int i) {
    if(buffer[i] == 0)
        return 1;
    else
        return 0;
}

/*
 * Returns 1 if file has been marked for deletion, else returns 0
 */
int isDeletedFile(unsigned char *buffer, int i){
    if (buffer[i] == 0xE5) {
        return 1;
    }
    else
        return 0;
}

/*
 * Returns 1 if is a directory, else returns 0
 */
int isDir(unsigned char *buffer,int i){
    if ((((buffer[i + 11] >> 4) & 1) == 1) && (!isDeletedFile(buffer,i))) {
        return 1;
    }
    else
        return 0;
}

/*
 * Returns 1 if valid filename, else returns 0
 */
int isValidName(unsigned char *buffer, int i,int length){
    int k = i;
    int a = 1;
    for(k=i;k<length;k++){
        if(buffer[k]==0x22 || buffer[k]==0x2a || buffer[k]==0x2b || buffer[k] == 0x2c||buffer[k]==0x2e ||buffer[k]==0x2f||buffer[k]==0x3a||buffer[k]==0x3b||buffer[k]==0x3c||buffer[k]==0x3d||buffer[k]== 0x3e||buffer[k]==0x3f||buffer[k]==0x5b||buffer[k]==0x5c||buffer[k]==0x5d||buffer[k]==0x7c)
            a = 0;
        else
            a = 1;
    }
    return a;
}

/*
 * Gets the name of the directory and stores it in char array
 * Calls trimwhitespace to trim the trailing white space
 */
char* getName(unsigned char *buffer,int i){
    char *name = (char *)malloc(8);
    int j = 0;
    int k = i;
    if (buffer[i] == 0x20) {
        return name;
    }
    else {
        while (j < 8) {
        name[j] = buffer[k];
        k++;
        j++;
       }
    }
    return trimtrailwhitespace(name);
}

/*
 * Gets the extension of the file and stores it in char array
 */
char* getExt(unsigned char *buffer,int i){
     char *ext = (char *)malloc(8);
    int j = 0;
    int k = i+8;
    if (buffer[k] == 0x20) {
        return ext;
    }
    else {
        while (j < 3) {
            ext[j] = buffer[k];
            k++;
            j++;
        }
    }
    return trimtrailwhitespace(ext);
}


/*
 * Returns the first cluster number of a file/directory
 */
int getFirstCluster(unsigned char *buffer,int i) {
    return (buffer[i+27] << 8) | buffer[i+26];
}

/*
 * Returns the size of a file or directory
 */
int getSize (unsigned char *buffer,int i) {
    return (buffer[i+31] << 24) | (buffer[i+30] << 16) | (buffer[i+29] << 8) | (buffer[i+28]);
}

/*
 * Prints the name of the given directory or file depending on the extension
 */
void printname(char *name,char *ext) {
    if(ext != NULL) {
        strcat(name,".");
        strcat(name, ext);
        printf("%s",name);
    }
    else
        printf("%s",name);
    }

/*
 *Prints the given cluster number
 */
void printclusternumber(int clusternum){
    printf(" |Starting Cluster : %d ",clusternum);
}

/*
 *Prints the size
 */
void printsize(int size){
    printf("|Size : %d Bytes",size);
}

/*
 * Prints a newline
 */
void printnewline() {
    printf("\n");
}

/*
 Read all the root directory entries and output information
*/
void readRootDirEntries(unsigned char *buffer,size_t bufsize) {
    int i;
    for (i = 0; i < bufsize; i = i + ROOTDIR_ENTRY_SIZE) {
        char *name = (char *) malloc (8);
        
        int firstClusterNum, size;
    
        if(isDirEnd(buffer,i))
            break;
        else if (isDir(buffer,i)) {
            name = getName(buffer,i);
            firstClusterNum = getFirstCluster(buffer,i);
            size = getSize(buffer,i);
            printname(name,NULL);
            printf(" - This is a directory");
            printclusternumber(firstClusterNum);
            printsize(size);
            printnewline();
        }
        else if(!isDeletedFile(buffer, i)) {
            char *ext = (char *) malloc (3);
            name = getName(buffer,i);
            ext = getExt(buffer,i);
            firstClusterNum = getFirstCluster(buffer,i);
            size = getSize(buffer,i);
            printname(name,ext);
            printclusternumber(firstClusterNum);
            printsize(size);
            printnewline();
        }
        else
            continue;
        }
    }
    
    
    
    
