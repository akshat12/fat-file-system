Name : Akshat Divekar

A C Program to read a Fat File System

Output for the program:

------------------------------------------------------------------------------
Reading the Boot Sector...
------------------------------------------------------------------------------
Sector Size: 512 
Cluster Size in Sectors: 4 
Number of Root Directory Entries: 512 
Numbers of Sectors per FAT : 4
Number Reserved Sectors : 1
Number of Hidden Sectors : 0
Sector Number of first copy of FAT : 1
Sector number of first sector of root directory : 9
Sector number of first sector of the first usable data cluster : 41
------------------------------------------------------------------------------
Reading the Root Directory Entries...
------------------------------------------------------------------------------
ASS1 - This is a directory |Starting Cluster : 3 |Size : 0 Bytes
ASS2 - This is a directory |Starting Cluster : 4 |Size : 0 Bytes
ASS3 - This is a directory |Starting Cluster : 5 |Size : 0 Bytes
SLIDES1.PDF |Starting Cluster : 21 |Size : 313638 Bytes
MALADE.TXT |Starting Cluster : 176 |Size : 13215 Bytes
FORM.PS |Starting Cluster : 180 |Size : 13543 Bytes
DIRECT0 - This is a directory |Starting Cluster : 191 |Size : 0 Bytes
LARGEDIR - This is a directory |Starting Cluster : 198 |Size : 0 Bytes
------------------------------------------------------------------------------
Reading the entire File System...
------------------------------------------------------------------------------
/
/ASS1 || List of Clusters: 3
/ASS1/.
/ASS1/..
/ASS1/ORDER.C ||List of Clusters: 6
/ASS1/MSCHEME.TXT ||List of Clusters: 7,8
/ASS1/BUBBLE.S ||List of Clusters: 14
/ASS2 || List of Clusters: 4
/ASS2/.
/ASS2/..
/ASS2/MSCHEME.TXT ||List of Clusters: 9
/ASS3 || List of Clusters: 5
/ASS3/.
/ASS3/..
/ASS3/MSCHEME1.TXT ||List of Clusters: 10
/ASS3/MSCHEME2.TXT ||List of Clusters: 11
/ASS3/MSCHEME3.TXT ||List of Clusters: 12,13
/SLIDES1.PDF ||List of Clusters: 21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174
/MALADE.TXT ||List of Clusters: 176,177,178,179,187,188,189
/FORM.PS ||List of Clusters: 180,181,182,183,184,185,186
/DIRECT0 || List of Clusters: 191
/DIRECT0/.
/DIRECT0/..
/DIRECT0/DIRECT1 || List of Clusters: 192
/DIRECT0/DIRECT1/.
/DIRECT0/DIRECT1/..
/DIRECT0/DIRECT1/DIRECT2 || List of Clusters: 193
/DIRECT0/DIRECT1/DIRECT2/.
/DIRECT0/DIRECT1/DIRECT2/..
/DIRECT0/DIRECT1/DIRECT2/DIRECT3 || List of Clusters: 194
/DIRECT0/DIRECT1/DIRECT2/DIRECT3/.
/DIRECT0/DIRECT1/DIRECT2/DIRECT3/..
/DIRECT0/DIRECT1/DIRECT2/DIRECT3/DIRECT4 || List of Clusters: 195
/DIRECT0/DIRECT1/DIRECT2/DIRECT3/DIRECT4/.
/DIRECT0/DIRECT1/DIRECT2/DIRECT3/DIRECT4/..
/DIRECT0/DIRECT1/DIRECT2/DIRECT3/DIRECT4/DEEPFILE.TXT ||List of Clusters: 197
/LARGEDIR || List of Clusters: 198,261,326
/LARGEDIR/.
/LARGEDIR/..
/LARGEDIR/FILE00.TXT ||List of Clusters: 199
/LARGEDIR/FILE01.TXT ||List of Clusters: 200
/LARGEDIR/FILE02.TXT ||List of Clusters: 201
/LARGEDIR/FILE03.TXT ||List of Clusters: 202
/LARGEDIR/FILE04.TXT ||List of Clusters: 203
/LARGEDIR/FILE05.TXT ||List of Clusters: 204
/LARGEDIR/FILE06.TXT ||List of Clusters: 205
/LARGEDIR/FILE10.TXT ||List of Clusters: 206
/LARGEDIR/FILE11.TXT ||List of Clusters: 207
/LARGEDIR/FILE12.TXT ||List of Clusters: 208
/LARGEDIR/FILE13.TXT ||List of Clusters: 209
/LARGEDIR/FILE14.TXT ||List of Clusters: 210
/LARGEDIR/FILE15.TXT ||List of Clusters: 211
/LARGEDIR/FILE16.TXT ||List of Clusters: 212
/LARGEDIR/FILE20.TXT ||List of Clusters: 213
/LARGEDIR/FILE21.TXT ||List of Clusters: 214
/LARGEDIR/FILE22.TXT ||List of Clusters: 215
/LARGEDIR/FILE23.TXT ||List of Clusters: 216
/LARGEDIR/FILE24.TXT ||List of Clusters: 217
/LARGEDIR/FILE25.TXT ||List of Clusters: 218
/LARGEDIR/FILE26.TXT ||List of Clusters: 219
/LARGEDIR/FILE30.TXT ||List of Clusters: 220
/LARGEDIR/FILE31.TXT ||List of Clusters: 221
/LARGEDIR/FILE32.TXT ||List of Clusters: 222
/LARGEDIR/FILE33.TXT ||List of Clusters: 223
/LARGEDIR/FILE34.TXT ||List of Clusters: 224
/LARGEDIR/FILE35.TXT ||List of Clusters: 225
/LARGEDIR/FILE36.TXT ||List of Clusters: 226
/LARGEDIR/FILE40.TXT ||List of Clusters: 227
/LARGEDIR/FILE41.TXT ||List of Clusters: 228
/LARGEDIR/FILE42.TXT ||List of Clusters: 229
/LARGEDIR/FILE43.TXT ||List of Clusters: 230
/LARGEDIR/FILE44.TXT ||List of Clusters: 231
/LARGEDIR/FILE45.TXT ||List of Clusters: 232
/LARGEDIR/FILE46.TXT ||List of Clusters: 233
/LARGEDIR/FILE50.TXT ||List of Clusters: 234
/LARGEDIR/FILE51.TXT ||List of Clusters: 235
/LARGEDIR/FILE52.TXT ||List of Clusters: 236
/LARGEDIR/FILE53.TXT ||List of Clusters: 237
/LARGEDIR/FILE54.TXT ||List of Clusters: 238
/LARGEDIR/FILE55.TXT ||List of Clusters: 239
/LARGEDIR/FILE56.TXT ||List of Clusters: 240
/LARGEDIR/FILE60.TXT ||List of Clusters: 241
/LARGEDIR/FILE61.TXT ||List of Clusters: 242
/LARGEDIR/FILE62.TXT ||List of Clusters: 243
/LARGEDIR/FILE63.TXT ||List of Clusters: 244
/LARGEDIR/FILE64.TXT ||List of Clusters: 245
/LARGEDIR/FILE65.TXT ||List of Clusters: 246
/LARGEDIR/FILE66.TXT ||List of Clusters: 247
/LARGEDIR/FILE07.TXT ||List of Clusters: 248
/LARGEDIR/FILE08.TXT ||List of Clusters: 249
/LARGEDIR/FILE17.TXT ||List of Clusters: 250
/LARGEDIR/FILE18.TXT ||List of Clusters: 251
/LARGEDIR/FILE27.TXT ||List of Clusters: 252
/LARGEDIR/FILE28.TXT ||List of Clusters: 253
/LARGEDIR/FILE37.TXT ||List of Clusters: 254
/LARGEDIR/FILE38.TXT ||List of Clusters: 255
/LARGEDIR/FILE47.TXT ||List of Clusters: 256
/LARGEDIR/FILE48.TXT ||List of Clusters: 257
/LARGEDIR/FILE57.TXT ||List of Clusters: 258
/LARGEDIR/FILE58.TXT ||List of Clusters: 259
/LARGEDIR/FILE67.TXT ||List of Clusters: 260
/LARGEDIR/FILE68.TXT ||List of Clusters: 262
/LARGEDIR/FILE70.TXT ||List of Clusters: 263
/LARGEDIR/FILE71.TXT ||List of Clusters: 264
/LARGEDIR/FILE72.TXT ||List of Clusters: 265
/LARGEDIR/FILE73.TXT ||List of Clusters: 266
/LARGEDIR/FILE74.TXT ||List of Clusters: 267
/LARGEDIR/FILE75.TXT ||List of Clusters: 268
/LARGEDIR/FILE76.TXT ||List of Clusters: 269
/LARGEDIR/FILE80.TXT ||List of Clusters: 270
/LARGEDIR/FILE81.TXT ||List of Clusters: 271
/LARGEDIR/FILE82.TXT ||List of Clusters: 272
/LARGEDIR/FILE83.TXT ||List of Clusters: 273
/LARGEDIR/FILE84.TXT ||List of Clusters: 274
/LARGEDIR/FILE85.TXT ||List of Clusters: 275
/LARGEDIR/FILE86.TXT ||List of Clusters: 276
/LARGEDIR/FILE000.TXT ||List of Clusters: 277
/LARGEDIR/FILE001.TXT ||List of Clusters: 278
/LARGEDIR/FILE002.TXT ||List of Clusters: 279
/LARGEDIR/FILE003.TXT ||List of Clusters: 280
/LARGEDIR/FILE004.TXT ||List of Clusters: 281
/LARGEDIR/FILE005.TXT ||List of Clusters: 282
/LARGEDIR/FILE006.TXT ||List of Clusters: 283
/LARGEDIR/FILE007.TXT ||List of Clusters: 284
/LARGEDIR/FILE008.TXT ||List of Clusters: 285
/LARGEDIR/FILE010.TXT ||List of Clusters: 286
/LARGEDIR/FILE011.TXT ||List of Clusters: 287
/LARGEDIR/FILE012.TXT ||List of Clusters: 288
/LARGEDIR/FILE013.TXT ||List of Clusters: 289
/LARGEDIR/FILE014.TXT ||List of Clusters: 290
/LARGEDIR/FILE015.TXT ||List of Clusters: 291
/LARGEDIR/FILE016.TXT ||List of Clusters: 292
/LARGEDIR/FILE017.TXT ||List of Clusters: 293
/LARGEDIR/FILE018.TXT ||List of Clusters: 294
/LARGEDIR/FILE020.TXT ||List of Clusters: 295
/LARGEDIR/FILE021.TXT ||List of Clusters: 296
/LARGEDIR/FILE022.TXT ||List of Clusters: 297
/LARGEDIR/FILE023.TXT ||List of Clusters: 298
/LARGEDIR/FILE024.TXT ||List of Clusters: 299
/LARGEDIR/FILE025.TXT ||List of Clusters: 300
/LARGEDIR/FILE026.TXT ||List of Clusters: 301
/LARGEDIR/FILE027.TXT ||List of Clusters: 302
/LARGEDIR/FILE028.TXT ||List of Clusters: 303
/LARGEDIR/FILE030.TXT ||List of Clusters: 304
/LARGEDIR/FILE031.TXT ||List of Clusters: 305
/LARGEDIR/FILE032.TXT ||List of Clusters: 306
/LARGEDIR/FILE033.TXT ||List of Clusters: 307
/LARGEDIR/FILE034.TXT ||List of Clusters: 308
/LARGEDIR/FILE035.TXT ||List of Clusters: 309
/LARGEDIR/FILE036.TXT ||List of Clusters: 310
/LARGEDIR/FILE037.TXT ||List of Clusters: 311
/LARGEDIR/FILE038.TXT ||List of Clusters: 312
/LARGEDIR/FILE040.TXT ||List of Clusters: 313
/LARGEDIR/FILE041.TXT ||List of Clusters: 314
/LARGEDIR/FILE042.TXT ||List of Clusters: 315
/LARGEDIR/FILE043.TXT ||List of Clusters: 316
/LARGEDIR/FILE044.TXT ||List of Clusters: 317
/LARGEDIR/FILE045.TXT ||List of Clusters: 318
/LARGEDIR/FILE046.TXT ||List of Clusters: 319
/LARGEDIR/FILE047.TXT ||List of Clusters: 320
/LARGEDIR/FILE048.TXT ||List of Clusters: 321
/LARGEDIR/FILE050.TXT ||List of Clusters: 322
/LARGEDIR/FILE051.TXT ||List of Clusters: 323
/LARGEDIR/FILE052.TXT ||List of Clusters: 324
/LARGEDIR/FILE053.TXT ||List of Clusters: 325
/LARGEDIR/FILE054.TXT ||List of Clusters: 327
/LARGEDIR/FILE055.TXT ||List of Clusters: 328
/LARGEDIR/FILE056.TXT ||List of Clusters: 329
/LARGEDIR/FILE057.TXT ||List of Clusters: 330
/LARGEDIR/FILE058.TXT ||List of Clusters: 331
/LARGEDIR/FILE060.TXT ||List of Clusters: 332
/LARGEDIR/FILE061.TXT ||List of Clusters: 333
/LARGEDIR/FILE062.TXT ||List of Clusters: 334
/LARGEDIR/FILE063.TXT ||List of Clusters: 335
/LARGEDIR/FILE064.TXT ||List of Clusters: 336
/LARGEDIR/FILE065.TXT ||List of Clusters: 337
/LARGEDIR/FILE066.TXT ||List of Clusters: 338
/LARGEDIR/FILE067.TXT ||List of Clusters: 339
/LARGEDIR/FILE068.TXT ||List of Clusters: 340
/LARGEDIR/FILE070.TXT ||List of Clusters: 341
/LARGEDIR/FILE071.TXT ||List of Clusters: 342
/LARGEDIR/FILE072.TXT ||List of Clusters: 343
/LARGEDIR/FILE073.TXT ||List of Clusters: 344
/LARGEDIR/FILE074.TXT ||List of Clusters: 345
/LARGEDIR/FILE075.TXT ||List of Clusters: 346
/LARGEDIR/FILE076.TXT ||List of Clusters: 347
/LARGEDIR/FILE080.TXT ||List of Clusters: 348
/LARGEDIR/FILE081.TXT ||List of Clusters: 349
/LARGEDIR/FILE082.TXT ||List of Clusters: 350
/LARGEDIR/FILE083.TXT ||List of Clusters: 351
/LARGEDIR/FILE084.TXT ||List of Clusters: 352
/LARGEDIR/FILE085.TXT ||List of Clusters: 353
/LARGEDIR/FILE086.TXT ||List of Clusters: 354
------------------------------------------------------------------------------
------------------------------------------------------------------------------