CC = gcc
CFLAGS = -g -O2 -Wall -Wstrict-prototypes

SRC = fat12info.c direntry.c directory.c fat.c fatfs.c 
OBJ = $(SRC:.c=.o)

fat12info: $(OBJ)
	gcc -o fat12info $(OBJ)
depend: $(SRC)
	makedepend $(SRC)

#clean:
#	/bin/rm -f $(OBJ) fatinfo *~

# DO NOT DELETE

direntry.o: direntry.h
directory.o: directory.h
fatfs.o: fatfs.h
fat.o : fat.h	