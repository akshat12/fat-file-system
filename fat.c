//
//  fat.c
//  FatFileSystem
//
//  Created by Axat on 2012-11-19.
//  Copyright (c) 2012 Axat. All rights reserved.
//



/*
 * Process the fat and stores all the entries in an integer array (FAT table)
 *
 */
void processFat(unsigned int *fat,unsigned char *fatbuffer, int bufsize) {
    
    int i,j = 0;
    
    for (i=0; i< bufsize; i = i + 3) {
        unsigned int fatval = (fatbuffer[i+2] << 16) | (fatbuffer[i + 1] << 8) | fatbuffer[i];
        fat[j] = fatval % 4096;
        fat[j+1] = fatval / 4096;
        j = j + 2;
    
}
}