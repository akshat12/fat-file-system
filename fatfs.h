/*
 * fatfs.h
 *
 * Definition of the structure used to represent a FAT filesystem.
 */

#ifndef FATFS_H
#define FATFS_H

#include <stdio.h>
#include <stdlib.h>

/* Add stuff in here */
/*
 Constants representing information about the FAT file system
 */

#define ROOTDIR_ENTRY_SIZE 32  //bytes
#define BOOT_BUFFER_SIZE 512   //bytes

/*
 * Structure used to store information about the FAT file system.
 */
typedef struct fatfsinfo
{
    /* The Sector Size. */
    int sectorSize;
    
    /* The Cluster Size. */
    int clusterSize;
    
    /* The number of root Directory Entries. */
    int rootDirEntries;
    
    /*The Size of a FAT in sectors*/
    int fatSectorSize;
    
    /*The number of reserved sectors*/
    int reservedSectors;
    
    /*The number of hidden sectors*/
    int hiddenSectors;
    
    /*The sector number of the first copy of the FAT*/
    int firstFATSectorNumber;
    
    /*The number of FAT copies*/
    int numberFATCopies;
    
    /*The first sector number of the root sector*/
    int firstSecNumberRoot;
    
    /*The size of the root directory in sectors*/
    int rootDirSectorSize;
    
    /*The Sector Number of the first Data Cluster*/
    int sectorNumberDataCluster;
    
    /*The Size of the root directory in bytes*/
    int rootDirSizeBytes;
    
    /*The size of a cluster in bytes*/
    int clusterSizeBytes;
} fatfsinfo;

/*
 * A function which gets all of the boot sector info from fat_image.dat
 */
void readBootSector(fatfsinfo *fatfsinfo,unsigned char *buffer);

#endif
