//
//  main.c
//  FatFileSystem
//
//

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include "fatfs.h"
#include "fat.h"
#include "direntry.h"
#include "directory.h"

/*
 * A function which prints a separator line
 */
void printseparator(void){
    printf("------------------------------------------------------------------------------\n");
}


/*
 * A function which initializes a buffer with the given size in bytes
 */
unsigned char* initbuffer(size_t size) {
    unsigned char *buffer = (unsigned char *) malloc (size * sizeof(char));
    return buffer;
}

int readFat(int fd) {
    printseparator();

    /*-----------------------------------------------------------------------------------
     Part 1 : Read the boot sector
     -------------------------------------------------------------------------------------*/
    printf("Reading the Boot Sector...\n");
    printseparator();
    
    fatfsinfo fatfsinfo;
    
    //Allocate Buffer with an intial size of 512 bytes to store the boot sector
    unsigned char *bootBuffer = initbuffer(BOOT_BUFFER_SIZE);
    
    //Read the first 512 bytes (boot sector) into the bootBuffer and check if there is an error
    if(read(fd,bootBuffer,BOOT_BUFFER_SIZE) != BOOT_BUFFER_SIZE)
        return 1;
    else
        //Call function readBootSector
        readBootSector(&fatfsinfo,bootBuffer);
    
    printseparator();
	
    /*----------------------------------------------------------------------------------
     Store the FAT into the "FAT" array
     ----------------------------------------------------------------------------------*/
    int fatSize = fatfsinfo.fatSectorSize * fatfsinfo.sectorSize;
    unsigned char *fatbuffer = initbuffer(fatSize);
    unsigned int Fat[fatSize];
    
    //Set the seek to where the first FAT begins and read in the FAT table entries
    if(lseek(fd,fatfsinfo.firstFATSectorNumber * fatfsinfo.sectorSize,SEEK_SET) < 0) return 1;
    
    //Read in all the FAT entries
    if(read(fd,fatbuffer,fatSize) != fatSize)  return 1;
    
    processFat(Fat,fatbuffer,fatSize);
    
    free(fatbuffer);
    
    /*-----------------------------------------------------------------------------------
     Part 2 : Read Root Directory Entries
     -------------------------------------------------------------------------------------*/
    printf("Reading the Root Directory Entries...\n");
    printseparator();
    
    //New buffer to read and store ALL the root directory entries
    //Size of root directory in bytes = rootDirSectorSize * sectorSize
    unsigned char *rootDirbuffer = initbuffer(fatfsinfo.rootDirSizeBytes);
    
    //Set the seek to where the root directory entries begin and read in ALL the root directory entries
    if(lseek(fd,fatfsinfo.firstSecNumberRoot * fatfsinfo.sectorSize,SEEK_SET) < 0) return 1;
    
    //Read in all the root directory entries
    if(read(fd,rootDirbuffer,fatfsinfo.rootDirSizeBytes) != fatfsinfo.rootDirSizeBytes)  return 1;
    
    //Call function readRootDirEntries to iterate over all the root directory entries
    readRootDirEntries(rootDirbuffer, fatfsinfo.rootDirSizeBytes);
    
    printseparator();
    
    /*--------------------------------------------------------------------------------------
     Part 3 : Traverse the File System
     ----------------------------------------------------------------------------------------*/
    printf("Reading the entire File System...\n");
    printseparator();
    printf("/\n"); //To indicate root
    traverseFat("/",fd,rootDirbuffer,0,Fat,&fatfsinfo);
    
    free(rootDirbuffer);
    
    printseparator();
    printseparator();
    
    return 0;
}


/*
 * The main method for the program
 */
int main(int argc, const char * argv[])
{
    if(argc != 2) {
        /* We print argv[0] assuming it is the program name */
        printf( "usage: %s filename \n", argv[0] );
    }
    else {
        

    int  fd = 0; //File Descriptor
    fd = open(argv[1],O_RDWR); //Open the file
        
        if (fd < -1)
            return 1;  // If the fd == -1, return 1 (signifies some error)
     
        readFat(fd);
    }
    return 0;
}

    