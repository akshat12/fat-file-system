//
//  directory.h
//  FatFileSystem
//
//  Created by Axat on 2012-11-21.
//  Copyright (c) 2012 Axat. All rights reserved.
//


#ifndef DIRECTORY_H
#define DIRECTORY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include "fatfs.h"
#include "direntry.h"


/*
 * 1 if lastCluster else 0
 */
int islastcluster(int clusternum,unsigned int *fat);

/*
 *  Get next Cluster given the current cluster
 */
int getnextcluster(int clusternum,unsigned int *fat);

/*
 * List all clusters
 */
void listclusters(int clusternum,unsigned int *fat);

//Copies the cluster to a buffer
void copycluster(int fd,int clusternumber,unsigned char  *clusterbuffer,fatfsinfo *fatfsinfo);

/*
 * Makes a dir path
 */
char* makeDirpath(char *parent, char *child,int size);

/*
 * Makes and prints a file path
 */
void printFilePath(char *pname,char *filename, char *ext);

/*
 Read all the directory entries and output information
 */
void traverseFat(char *pName, int fd,unsigned char *buffer,int clusternumber,unsigned int *Fat,fatfsinfo *fatfsinfo);

#endif
